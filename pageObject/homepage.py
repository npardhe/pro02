from selenium.webdriver.common.by import By


class HomePage:
    def __init__(self, driver):
        self.driver=driver

    noauth=(By.XPATH,"//button[@class='_2KpZ6l _2doB4z']")
    search=(By.CSS_SELECTOR,"input[class='_3704LK']")
    search_click=(By.CSS_SELECTOR,"button[class='L0Z3Pu']")

    def get_noauth(self):
        return self.driver.find_element(*HomePage.noauth)

    def get_search(self):
        return self.driver.find_element(*HomePage.search)
    def get_search_click(self):
        return self.driver.find_element(*HomePage.search_click)