import time

from pageObject.homepage import HomePage
from utilities.baseclass import BaseClass


class TestOne(BaseClass):
    def test_flipkart(self):
        log=self.getlogger()
        homeObj=HomePage(self.driver)
        homeObj.get_noauth().click()
        homeObj.get_search().send_keys("one plus nord 2")
        homeObj.get_search_click().click()
        time.sleep(2)