import time

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
import pytest

@pytest.fixture(scope="class")
def setup(request):
    obj=Service("/home/ni3/selenium/chromedriver_linux64/chromedriver")
    driver=webdriver.Chrome(service=obj)
    driver.get("https://www.flipkart.com/")
    request.cls.driver=driver
    yield
    driver.close()