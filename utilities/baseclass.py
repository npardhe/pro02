import logging
import pytest
import inspect

@pytest.mark.usefixtures("setup")
class BaseClass():
    def getlogger(self):
        loggername=inspect.stack()[1][3]  # funtion ka naam log file m dene ke liye
        logger=logging.getLogger(loggername) # __name__ for give name in log file
        fileHandler=logging.FileHandler('logfile.log')
        formatter=logging.Formatter("%(asctime)s :%(levelname)s :%(name)s :%(message)s")
        fileHandler.setFormatter(formatter)
        logger.addHandler(fileHandler)
        logger.setLevel(logging.DEBUG)
        return logger